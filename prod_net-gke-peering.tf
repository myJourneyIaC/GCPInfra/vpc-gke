
variable "other_private_network_self_link" {
  description = "Link of the private db network"
  type        = string
}


#### Peering between GKE and Network Prod

resource "google_compute_network_peering" "gke-netprod-peering" {
  name         = "gke-netprod"
  network      = var.other_private_network_self_link 
  peer_network = google_compute_network.vpc.id
}

resource "google_compute_network_peering" "netprod-gke-peering" {
  name         = "netprod-gke"
  network      = google_compute_network.vpc.id
  peer_network = var.other_private_network_self_link 
}

