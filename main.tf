terraform {
  # This module has been updated with 0.12 syntax, which means it is no longer compatible with any versions below 0.12.
  required_version = ">= 0.12"
}

# ---------------------------------------------------------------------------------------------------------------------
# Create the Network & corresponding Router to attach other resources to
# Networks that preserve the default route are automatically enabled for Private Google Access to GCP services
# provided subnetworks each opt-in; in general, Private Google Access should be the default.
# ---------------------------------------------------------------------------------------------------------------------

resource "google_compute_network" "vpc" {
  name    = "${var.name_prefix}-network"
  project = var.project

  # Always define custom subnetworks- one subnetwork per region isn't useful for an opinionated setup
  auto_create_subnetworks = "false"

  # A global routing mode can have an unexpected impact on load balancers; always use a regional mode
  routing_mode = "REGIONAL"
}

resource "google_compute_router" "vpc_router" {
  name = "${var.name_prefix}-router"

  project = var.project
  region  = var.region
  network = google_compute_network.vpc.self_link
}

# ---------------------------------------------------------------------------------------------------------------------
# Public Subnetwork Config
# Public internet access for instances with addresses is automatically configured by the default gateway for 0.0.0.0/0
# External access is configured with Cloud NAT, which subsumes egress traffic for instances without external addresses
# ---------------------------------------------------------------------------------------------------------------------

resource "google_compute_subnetwork" "vpc_subnetwork_public" {
  name = "${var.name_prefix}-subnetwork-public"

  project = var.project
  region  = var.region
  network = google_compute_network.vpc.self_link

  private_ip_google_access = true
  ip_cidr_range            = cidrsubnet(var.cidr_block, var.cidr_subnetwork_width_delta, 0)

  secondary_ip_range {
    range_name = "public-services"
    ip_cidr_range = cidrsubnet(
      var.secondary_cidr_block,
      var.secondary_cidr_subnetwork_width_delta,
      1 * (1 + var.cidr_subnetwork_spacing)
    )
  }

  dynamic "log_config" {
    for_each = var.log_config == null ? [] : list(var.log_config)

    content {
      aggregation_interval = var.log_config.aggregation_interval
      flow_sampling        = var.log_config.flow_sampling
      metadata             = var.log_config.metadata
    }
  }
}

resource "google_compute_router_nat" "vpc_nat" {
  name = "${var.name_prefix}-nat"

  project = var.project
  region  = var.region
  router  = google_compute_router.vpc_router.name

  nat_ip_allocate_option = "AUTO_ONLY"

  # "Manually" define the subnetworks for which the NAT is used, so that we can exclude the public subnetwork
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  subnetwork {
    name                    = google_compute_subnetwork.vpc_subnetwork_private.self_link
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# Private Subnetwork Config
# ---------------------------------------------------------------------------------------------------------------------
resource "google_compute_subnetwork" "vpc_subnetwork_private" {
  count = var.target_subnetwork_private_gke ? 0 : 1
  name = "${var.name_prefix}-subnetwork-private"

  project = var.project
  region  = var.region
  network = google_compute_network.vpc.self_link

  private_ip_google_access = true

  ip_cidr_range = cidrsubnet(
    var.cidr_block,
    var.cidr_subnetwork_width_delta,
    1 * (1 + var.cidr_subnetwork_spacing)
  )

  secondary_ip_range {
    range_name = "private-net"
    ip_cidr_range = cidrsubnet(
      var.net_cidr_block,
      var.net_cidr_subnetwork_width_delta,
      1 * (1 + var.net_cidr_subnetwork_spacing)
    )
  }

  dynamic "log_config" {
    for_each = var.log_config == null ? [] : list(var.log_config)

    content {
      aggregation_interval = var.log_config.aggregation_interval
      flow_sampling        = var.log_config.flow_sampling
      metadata             = var.log_config.metadata
    }
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# Private Subnetwork Config: POD && SVC
# ---------------------------------------------------------------------------------------------------------------------

resource "google_compute_subnetwork" "vpc_subnetwork_private_gke" {
  count = var.target_subnetwork_private_gke ? 1 : 0
  name = "${var.name_prefix}-subnetwork-private"

  project = var.project
  region  = var.region
  network = google_compute_network.vpc.self_link

  private_ip_google_access = true

  ip_cidr_range = cidrsubnet(
    var.cidr_block,
    var.cidr_subnetwork_width_delta,
    1 * (1 + var.cidr_subnetwork_spacing)
  )

  secondary_ip_range {
    range_name = "private-pod"
    ip_cidr_range = cidrsubnet(
      var.pod_cidr_block,
      var.pod_cidr_subnetwork_width_delta,
      1 * (1 + var.pod_cidr_subnetwork_spacing)
    )
  }

  secondary_ip_range {
    range_name = "private-svc"
    ip_cidr_range = cidrsubnet(
      var.svc_cidr_block,
      var.svc_cidr_subnetwork_width_delta,
      1 * (1 + var.svc_cidr_subnetwork_spacing)
    )
  }

  dynamic "log_config" {
    for_each = var.log_config == null ? [] : list(var.log_config)

    content {
      aggregation_interval = var.log_config.aggregation_interval
      flow_sampling        = var.log_config.flow_sampling
      metadata             = var.log_config.metadata
    }
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# Private Subnetwork INTERNAL_LOAD_BALANCER
# ---------------------------------------------------------------------------------------------------------------------

resource "google_compute_subnetwork" "vpc_subnetwork_private_ilb" {
  name = "${var.name_prefix}-subnetwork-private-ilb"

  provider = google-beta
  project = var.project
  region  = var.region
  network = google_compute_network.vpc.self_link
  purpose = "INTERNAL_HTTPS_LOAD_BALANCER"
  role    = "ACTIVE"
  

  #private_ip_google_access = true

  ip_cidr_range = cidrsubnet(
    var.cidr_block_ilb,
    var.cidr_subnetwork_width_delta,
    var.cidr_subnetwork_spacing
  )

}

/*
resource "google_compute_forwarding_rule" "forwarding-rule-ilb" {
  name                  = "forwarding-rule-ilb"
  region                = var.region
  load_balancing_scheme = "INTERNAL"
  all_ports             = true
  allow_global_access   = true
  network               = google_compute_network.vpc.self_link
  subnetwork            = google_compute_subnetwork.vpc_subnetwork_private_ilb.name
  network_tier          = "PREMIUM"
  backend_service       = google_compute_region_backend_service.home.id
}

resource "google_compute_region_target_http_proxy" "default" {
  region  = var.region
  name    = "https-redirect-proxy"
  url_map = google_compute_region_url_map.region-url-map-ilb.id
}

resource "google_compute_region_url_map" "region-url-map-ilb" {
    region = var.region
    name   = "url-map-ilb"
    default_service = google_compute_region_backend_service.home.id

    host_rule {
      hosts        = [var.domain]
      path_matcher = "allpaths"
    }  

    path_matcher {
    name            = "allpaths"
    default_service = google_compute_region_backend_service.home.id

      path_rule {
        paths   = ["/home"]
        service = google_compute_region_backend_service.home.id
      }
    }
}

resource "google_compute_region_backend_service" "home" {
  name        = "home"
  protocol    = "HTTP"
  timeout_sec = 10

  health_checks = [google_compute_region_health_check.default.id]
  load_balancing_scheme = "INTERNAL_MANAGED"
}

resource "google_compute_region_health_check" "default" {
  name               = "health-check"
  http_health_check {
    port = 80
  }
}

*/

# ---------------------------------------------------------------------------------------------------------------------
# Adding Cloud SQL Support
# ---------------------------------------------------------------------------------------------------------------------

resource "google_compute_global_address" "private_ip_address" {

  name          = "gke-private-ip-address-${var.name_prefix}"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"

  address       =  cidrhost( cidrsubnet(var.svc_cidr_block,7,7),0 )
  prefix_length = 24
  network = google_compute_network.vpc.self_link
}

resource "google_service_networking_connection" "private_vpc_connection" {
  network = google_compute_network.vpc.self_link
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}

# ---------------------------------------------------------------------------------------------------------------------
# Attach Firewall Rules to allow inbound traffic to tagged instances
# ---------------------------------------------------------------------------------------------------------------------

module "network_firewall" {
  source = "../network-firewall"

  name_prefix = var.name_prefix

  project                               = var.project
  network                               = google_compute_network.vpc.self_link
  allowed_public_restricted_subnetworks = var.allowed_public_restricted_subnetworks

  public_subnetwork  = google_compute_subnetwork.vpc_subnetwork_public.self_link
  private_subnetwork = google_compute_subnetwork.vpc_subnetwork_private.self_link
}
